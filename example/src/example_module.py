import os
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


num_classes = 10
input_shape = (28, 28, 1)


def get_latest_ckpt(ckpt_dir):
    """ Load model weights from checkpoint directory. """
    ckpt_list = os.listdir(ckpt_dir)
    if len(ckpt_list) < 1:
        return None, None, -1
    file_name = sorted(ckpt_list, key=lambda x: int(x.split('.')[1]))[-1]
    full_path = os.path.join(ckpt_dir, file_name)
    ckpt_nr = int(file_name.split('.')[1])
    return file_name, full_path, ckpt_nr


def get_train_data():
    (x_train, y_train), _ = keras.datasets.mnist.load_data()
    x_train = x_train.astype("float32") / 255
    x_train = np.expand_dims(x_train, -1)
    y_train = keras.utils.to_categorical(y_train, num_classes)
    return x_train, y_train


def get_test_data():
    _, (x_test, y_test) = keras.datasets.mnist.load_data()
    x_test = x_test.astype("float32") / 255
    x_test = np.expand_dims(x_test, -1)
    y_test = keras.utils.to_categorical(y_test, num_classes)
    return x_test, y_test


def get_model(params):
    return keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten(),
            layers.Dropout(0.5),
            layers.Dense(num_classes, activation="softmax"),
        ]
    )


def evaluate(params):
    x_test, y_test = get_test_data()
    model = get_model(params)
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    _, ckpt_path, _ = get_latest_ckpt(params['CKPT_DIR'])
    model.load_weights(ckpt_path)
    score = model.evaluate(x_test, y_test, verbose=0)
    print(score)

    
def train(params):
    tf.random.set_seed(params['RANDOM_SEED'])
    np.random.seed(params['RANDOM_SEED'])

    # data
    x_train, y_train = get_train_data()

    # model
    model = get_model(params)
    model.summary()
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

    # load weights if training continues
    _, ckpt_path, ckpt_nr = get_latest_ckpt(params['CKPT_DIR'])
    if ckpt_nr >= 0:
        model.load_weights(ckpt_path)

    # fit model to data
    ckpt_callback = keras.callbacks.ModelCheckpoint(
        os.path.join(params['CKPT_DIR'], 'weights.{epoch:03d}.hdf5'),
        monitor='val_loss',
        save_weights_only=False,
        save_freq='epoch'
    )

    tb_callback = keras.callbacks.TensorBoard(
        log_dir=os.path.join(params['LOG_DIR'], 'tb'),
        histogram_freq=1,
        update_freq='epoch'
    )

    model.fit(
        x_train,
        y_train,
        initial_epoch=ckpt_nr,
        batch_size=params['BATCH_SIZE'],
        epochs=params['EPOCHS'],
        validation_split=0.1,
        callbacks=[ckpt_callback, tb_callback]
    )
