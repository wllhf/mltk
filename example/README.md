# Tutorial

## Docker
- Change directory to example directory
- Build a docker image using the given dockerfile:

    docker build ./ -t wllhf/yassmltk:tf_base
    

## Run experiment
The example folder contains a minimal experiment that trains a convolutional neural network 
with the help of Tensorflow on the MNIST data set. The experiment can be found in example/model.

The configuration file defines which model definition (example/src/example_model.py) and which 
docker image to use (yassmltk:tf_base). The one we built above. Change the parameter *SOURCE_DIR*
in config.yml to path_to_yassmltk_repo/example/src.

Run the experiment with:

    runexp path_to_yassmltk_repo/example/model
    
Use --cpu option if your system does not have a GPU. Use -m 6G if your system has less than 16GB of RAM.
