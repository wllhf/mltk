import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="yassmltk",
    version="0.0.7",
    author="wllhf",
    description="Yet another stupid, simple toolkit for ML experiments.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    #packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: Linux",
    ],
    python_requires='>=3.6', # dicts are implemented ordered, but are a language feature only from 3.7 on
    install_requires=['pyyaml', 'pandas'],
    packages=['yassmltk'],
    entry_points={
        'console_scripts': [
            'runexp = yassmltk.run_exp:main',
        ],
    },
)
