import os

import yaml

from . import constants as const


class Params(dict):

    def __init__(self, data: dict, **kwargs):
        super().__init__(data, **kwargs)

    @staticmethod
    def init_from_file(path: str):
        path = os.path.join(path, const.FILE_PARAMS)

        with open(path, 'r') as yaml_file:
            data = yaml.full_load(yaml_file)

        return Params(data)


class Grid(dict):

    def __init__(self, data: dict, **kwargs):
        super().__init__(data, **kwargs)

    @staticmethod
    def init_from_file(path: str):
        path = os.path.join(path, const.FILE_GRID)

        with open(path, 'r') as yaml_file:
            data = yaml.full_load(yaml_file)

        return Grid(data)
