import os

# files
FILE_CONFIG = 'config.yml'
FILE_PARAMS = 'params.yml'
FILE_GRID = 'grid.yml'
FILE_GRID_CSV = 'grid_params.csv'

# directories
DIR_COPY_SRC = 'src'
DIR_CKPT = 'ckpts'
DIR_LOGS = 'log'
DIR_EVAL = 'eval'
DIR_DATA = 'data'

# mount points in container
CTR_PATH_WORKDIR = '/wd'
CTR_PATH_EXP = os.path.join(CTR_PATH_WORKDIR, 'exp')
CTR_PATH_OUT = os.path.join(CTR_PATH_WORKDIR, 'out')
CTR_PATH_SRC = os.path.join(CTR_PATH_WORKDIR, DIR_COPY_SRC)
CTR_PATH_YASS = os.path.join(CTR_PATH_WORKDIR, 'yassmltk')  # changing this breaks imports in docker hook
CTR_PATH_DATA = os.path.join(CTR_PATH_WORKDIR, DIR_DATA)

# dictionary keys
KEY_TITLE = 'TITLE'
KEY_PROJECT_NAME = 'PROJECT_NAME'
KEY_EXP_NAME = 'EXPERIMENT_NAME'
KEY_DESCRIPTION = 'DESCRIPTION'

KEY_IMAGE_URL = 'IMAGE_URL'
KEY_MODULE = 'MODULE'
KEY_SOURCE_DIR = 'SOURCE_DIR'
KEY_YASS_DIR = 'YASSMLTK_DIR'
KEY_DATA_DIR = 'DATA_DIR'
KEY_EXP_DIR = 'EXPERIMENT_DIR'
KEY_ADDITIONAL_DIRS = 'ADDITIONAL_DIRS'

KEY_MODEL_DIR = 'MODEL_DIR'
KEY_CKPT_DIR = 'CKPT_DIR'
KEY_LOGS_DIR = 'LOG_DIR'
KEY_EVAL_DIR = 'EVAL_DIR'
