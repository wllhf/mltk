"""
Configuration constants and code.

This module is imported in the docker hook
   -> import only built-in packages.
"""
import os
import yaml
import collections

from .util import rlinput

import yassmltk.constants as const


class Config(collections.UserDict):

    REQUIRED_PROPERITES = [
        const.KEY_TITLE,
        const.KEY_DESCRIPTION,
        const.KEY_SOURCE_DIR,
        const.KEY_MODULE,
        const.KEY_IMAGE_URL
    ]

    def __init__(self, data: dict, check=False, **kwargs):
        if check:
            data = self.check_and_write(data)

        for required_property in Config.REQUIRED_PROPERITES:
            assert required_property in data,\
                f'Missing key \'{required_property}\' in config file.'

        super().__init__(data, **kwargs)

    def update_directories(self, current_model_dir):
        """
        Update path parameters to mount points in the container for the current run/model.
        """
        self[const.KEY_MODEL_DIR] = current_model_dir
        self[const.KEY_CKPT_DIR] = os.path.join(current_model_dir, const.DIR_CKPT)
        self[const.KEY_LOGS_DIR] = os.path.join(current_model_dir, const.DIR_LOGS)
        self[const.KEY_EVAL_DIR] = os.path.join(current_model_dir, const.DIR_EVAL)

        if const.KEY_DATA_DIR in self:
            self[const.KEY_DATA_DIR] = const.CTR_PATH_DATA

        if const.KEY_ADDITIONAL_DIRS in self:
            for additional_dir_key in self[const.KEY_ADDITIONAL_DIRS]:
                additional_dir = self[const.KEY_ADDITIONAL_DIRS][additional_dir_key]
                expanded_additional_dir = os.path.expanduser(additional_dir)
                self[const.KEY_ADDITIONAL_DIRS][additional_dir_key] = os.path.join(const.CTR_PATH_WORKDIR,
                                                                                   expanded_additional_dir)

    @staticmethod
    def check_and_write(data: dict):
        # hack to add keys in correct order (dicts are ordered in > 3.7)
        default_dict = {}
        for required_property in Config.REQUIRED_PROPERITES:
            default_dict[required_property] = ''

        conf = Config(default_dict, check=False)
        conf.update(data)

        for required_property in Config.REQUIRED_PROPERITES:
            if required_property not in conf or len(data[required_property]) == 0:
                data[required_property] = rlinput('Please add ' + required_property + ':')

        with open(os.path.join(conf[const.KEY_EXP_DIR], const.FILE_CONFIG), 'w') as ymlfile:
            yaml.dump(conf.data, ymlfile, sort_keys=False)

        return data

    @staticmethod
    def init_from_file(path: str, check=False):
        path = os.path.join(path, const.FILE_CONFIG)

        with open(path, 'r') as ymlfile:
            data = yaml.load(ymlfile, Loader=yaml.FullLoader)

        data[const.KEY_EXP_DIR] = os.path.expanduser(os.path.dirname(path))

        return Config(data, check=check)


