import os
import sys
import shutil
import inspect
import itertools

import yaml
import pandas as pd

from .util import query, query_ccc, rlinput
from .git import write_repo_info

from . import config as conf
from . import params as para
from . import constants as const


# these pattern should come from a more sane place
# (like a gitignore) but I didn't have time
IGNORE_PATTERNS = [
    '*#*', '*flycheck*', '*~', '__pycache__',
    '*.pyc', '*.npy', '*ipynb_checkpoints'
]


def copy_source(config, overwrite=False):

    dest_dir = os.path.join(config[const.KEY_EXP_DIR], const.DIR_COPY_SRC)
    if os.path.exists(dest_dir) and overwrite:
        shutil.rmtree(dest_dir)
    if not os.path.exists(dest_dir):
        shutil.copytree(
            config[const.KEY_SOURCE_DIR], dest_dir, ignore=shutil.ignore_patterns(*IGNORE_PATTERNS)
        )

    # write repository information --------------------------------------------
    write_repo_info(dest_dir, config[const.KEY_SOURCE_DIR])
    return dest_dir


def prepare_naive_grid_search(config):
    """
    Prepares a search over a list of meta parameters,
    whereas the meta parameters are considered independent.
    """
    params = para.Params.init_from_file(config[const.KEY_EXP_DIR])
    grid = para.Grid.init_from_file(config[const.KEY_EXP_DIR])

    nruns = sum([len(v) for k, v in grid.items()])
    print('Preparing naive grid search with', nruns, 'experiments ...')

    if not os.path.exists(os.path.join(config[const.KEY_EXP_DIR], 'models')):
        os.makedirs(os.path.join(config[const.KEY_EXP_DIR], 'models'))

    table = pd.DataFrame(columns=['directory'] + list(grid.keys()))
    model_dirs = []
    count = 0
    for key, values in grid.items():
        for elem in values:
            row = {'directory': str(count).zfill(max(4, len(str(nruns))+2))}
            path = os.path.join(config[const.KEY_EXP_DIR], 'models', row['directory'])
            
            if not os.path.exists(path):
                os.makedirs(path)

            current_params = params.copy()
            current_params[key] = elem
            for k in grid.keys():
                row[k] = current_params[k]
                
            with open(os.path.join(path, const.FILE_PARAMS), 'w') as ymlfile:
                yaml.dump(current_params, ymlfile, sort_keys=False)

            table = table.append(row, ignore_index=True)
            model_dirs.append(path)
            count += 1

    table.to_csv(os.path.join(config[const.KEY_EXP_DIR], const.FILE_GRID_CSV))
    return model_dirs


def prepare_full_grid_search(config):
    """
    Prepares an exhaustive search over a list of meta parameters,
    whereas all possible combinations of parameters are explored.
    """
    params = para.Params.init_from_file(config[const.KEY_EXP_DIR])
    grid = para.Grid.init_from_file(config[const.KEY_EXP_DIR])

    pairs = []
    for k, values in grid.items():
        pairs.append([])
        for v in values:
            pairs[-1].append((k, v))
    combinations = list(itertools.product(*pairs))
    print('Preparing grid search with', len(combinations), 'experiments ...')

    if not os.path.exists(os.path.join(config[const.KEY_EXP_DIR], 'models')):
        os.makedirs(os.path.join(config[const.KEY_EXP_DIR], 'models'))

    table = pd.DataFrame(columns=['directory'] + list(grid.keys()))
    model_dirs = []
    for i, elem in enumerate(combinations):
        row = {'directory': str(i).zfill(max(4, len(str(len(combinations)))+2))}
        path = os.path.join(config[const.KEY_EXP_DIR], 'models', row['directory'])

        if not os.path.exists(path):
            os.makedirs(path)

        current_params = params.copy()
        for key, value in elem:
            current_params[key] = value
            row[key] = value
            
        with open(os.path.join(path, const.FILE_PARAMS), 'w') as ymlfile:
            yaml.dump(current_params, ymlfile, sort_keys=False)

        table = table.append(row, ignore_index=True)
        model_dirs.append(path)

    table.to_csv(os.path.join(config[const.KEY_EXP_DIR], const.FILE_GRID_CSV))
    return model_dirs


def prepare_random_search(config):
    raise NotImplementedError


def prepare_training(config, ccc=None):
    """
    Prepares the model directory for the experiments and logs params, code base and caller.
    The copy of the params and config may seem overly redundant but is nice if you continue
    training with a different set of parameters.

    Args:
      config: dict global configuration
      ccc: string (default None)
       C[a]ncel, C[o]ntinue C[l]ear if model already exists? Query user if None.

    Returns:
      params: dictionary
       Parameter dictionary.
    """
    # load params file --------------------------------------------------------
    params = para.Params.init_from_file(config[const.KEY_MODEL_DIR])

    # make subfolders ---------------------------------------------------------
    if not os.path.exists(config[const.KEY_CKPT_DIR]):
        os.makedirs(config[const.KEY_CKPT_DIR])
    else:
        if ccc is None:
            ccc = query_ccc('Model already exists!')
        if ccc == 'a':
            sys.exit()
        if ccc == 'l':
            shutil.rmtree(config[const.KEY_CKPT_DIR])
            shutil.rmtree(config[const.KEY_LOGS_DIR])
            os.makedirs(config[const.KEY_CKPT_DIR])

    if not os.path.exists(config[const.KEY_LOGS_DIR]):
        os.makedirs(config[const.KEY_LOGS_DIR])

    # write info about caller
    # this is somehow obsolete but wasn't deleted as it could be replaced by
    # writing some info about the caller in a way that is meaningful even
    # when using yassmltk
    caller_function = inspect.stack()[1].function
    caller_filename = inspect.stack()[1].filename
    caller_filename = os.path.splitext(os.path.basename(caller_filename))[0]
    with open(os.path.join(config[const.KEY_LOGS_DIR], 'caller.txt'), 'ab') as fobj:
        fobj.write(b'\n')
        fobj.write(caller_filename.encode('ascii'))
        fobj.write(b'\n')
        fobj.write(caller_function.encode('ascii'))
        fobj.write(b'\n'+b'='*80+b'\n')

    # write config
    with open(os.path.join(config[const.KEY_LOGS_DIR], 'config.txt'), 'a') as fobj:
        fobj.write('\n')
        yaml.dump(config, fobj, sort_keys=False)
        fobj.write('\n'+'='*80+'\n')

    # write parameters
    with open(os.path.join(config[const.KEY_LOGS_DIR], 'params.txt'), 'a') as fobj:
        fobj.write('\n')
        yaml.dump(params, fobj, sort_keys=False)
        fobj.write('\n'+'='*80+'\n')

    return params


def prepare_evaluation(config, ccc=None):
    """
    Prepare evalutian directory.

    Args:
      ccc: string (default None)
       C[a]ncel, C[o]ntinue C[l]ear if model already exists? Query user if None.

    """
    # make subfolders ---------------------------------------------------------
    if not os.path.exists(config[const.KEY_EVAL_DIR]):
        os.makedirs(config[const.KEY_EVAL_DIR])
    else:
        if ccc is None:
            ccc = query_ccc('Evaluation already exists!')
        if ccc == 'a':
            sys.exit()
        if ccc == 'l':
            shutil.rmtree(config[const.KEY_EVAL_DIR])
            os.makedirs(config[const.KEY_EVAL_DIR])
