"""
This module is indirectly imported in the docker hook
   -> import only built-in packages.
"""
import os
import sys
import readline


def query(question):
    """
    Query user with yes/no question.
    """
    answers = {"yes": True, "Y": True, "no": False, "n": False}
    while True:
        sys.stdout.write(question)
        choice = input()
        if choice in answers:
            return answers[choice]

        sys.stdout.write("Respond with 'Y' or 'n'.\n")

        
def query_ccc(message):
    """
    Query user with yes/no question.
    """
    answers = ['a', 'o', 'l']
    while True:
        sys.stdout.write(message+" C[a]ncel, C[o]ntinue or C[l]ear? ")
        choice = input()
        if choice in answers:
            return choice

        sys.stdout.write("Respond with 'a', 'o' or 'l'.\n")


def rlinput(prompt, prefill=''):
    """
    Query user with prompt and default answer.
    """

    readline.set_startup_hook(lambda: readline.insert_text(prefill))
    try:
        return os.path.expanduser(input(prompt))
    finally:
        readline.set_startup_hook()
