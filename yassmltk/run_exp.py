#!/usr/local/bin/python
import os
import sys
import inspect
import argparse
import subprocess

import yaml

import yassmltk
from yassmltk.util import query
from yassmltk.init import copy_source
from yassmltk.init import prepare_naive_grid_search, prepare_full_grid_search
from yassmltk.init import prepare_training, prepare_evaluation


import yassmltk.constants as const
import yassmltk.config as conf


def run_experiment(args):
    """
    To make sure that every sub-experiment has a clean state and fresh random
    seed, in a simple, stupid way, we launch a new container for every experiment.
    As this launch can happen hours after the initial start of the script, we
    copy the source files to make sure nobody changes them in the meantime.

    Args:
    args: argparse object
    """
    ccc = 'o'
    if args.cancel and args.clear:
        raise ValueError("--cancel and --clear arguments are mutually exclusive")
    elif args.cancel:
        ccc = 'a'
    elif args.clear:
        ccc = 'l'
    elif args.query:
        ccc = None

    # prepare experiment ------------------------------------------------------
    yassmltk_dir = os.path.dirname(inspect.getfile(yassmltk))
    config = conf.Config.init_from_file(args.directory, check=True)

    # copy source -------------------------------------------------------------
    src_copy_dir = copy_source(config, args.clear)

    # prepare grid search -----------------------------------------------------
    do_grid_search = os.path.exists(os.path.join(args.directory, const.FILE_GRID))
    if do_grid_search:
        if args.naive_grid:
            model_dirs = prepare_naive_grid_search(config)
        else:
            model_dirs = prepare_full_grid_search(config)
    else:
        model_dirs = [args.directory]

    # prepare to spawn a container --------------------------------------------
    docker_cmd = ['docker', 'run', '-it', '--rm']
    docker_cmd += ['-e', 'PYTHONHASHSEED=0']
    docker_cmd += ['-e', 'PYTHONPATH=' + const.CTR_PATH_WORKDIR]
    docker_cmd += ['--ipc=host']
    docker_cmd += ['--gpus', 'all'] if not args.cpu else []
    docker_cmd += ['--user', str(os.geteuid())+':'+str(os.getegid())]
    # RAM (equal values turn off swap)
    docker_cmd += ['--memory', args.memory, '--memory-swap', args.memory]
    # work directory
    docker_cmd += ['--workdir', const.CTR_PATH_WORKDIR]
    # yassmltk directory
    docker_cmd += ['--volume', yassmltk_dir + ':' + const.CTR_PATH_YASS]
    # experiment directory
    docker_cmd += ['--volume', config[const.KEY_EXP_DIR] + ':' + const.CTR_PATH_EXP]
    # source directory
    docker_cmd += ['--volume', src_copy_dir + ':' + const.CTR_PATH_SRC]
    # data directory
    if const.KEY_DATA_DIR in config:
        docker_cmd += ['--volume', os.path.expanduser(config[const.KEY_DATA_DIR]) + ':' + const.CTR_PATH_DATA]
    # additional directories
    if const.KEY_ADDITIONAL_DIRS in config:
        for additional_dir_key in config[const.KEY_ADDITIONAL_DIRS]:
            additional_dir = config[const.KEY_ADDITIONAL_DIRS][additional_dir_key]
            expanded_additional_dir = os.path.expanduser(additional_dir)
            docker_cmd += ['--volume', expanded_additional_dir + ':' +
                           os.path.join(const.CTR_PATH_WORKDIR, expanded_additional_dir)]

    # start experiments -------------------------------------------------------
    print("Starting experiments ...")
    for i, model_dir in enumerate(model_dirs):

        config.update_directories(model_dir)
        prepare_training(config, ccc=ccc)
        prepare_evaluation(config, ccc=ccc)

        command = docker_cmd + ['--volume', model_dir + ':' + const.CTR_PATH_OUT] + [config[const.KEY_IMAGE_URL]]
        command += ['python3', const.CTR_PATH_YASS + '/docker_hook.py']
        command += ['-ot'] if args.omit_training else []
        command += ['-oe'] if args.omit_evaluation else []

        print(str(i+1)+'/'+str(len(model_dirs))+':', model_dir)
        if args.redirect_output:
            with open(os.path.join(args.directory, 'stdout.log'), 'w') as std,\
                 open(os.path.join(args.directory, 'errout.log'), 'w') as err:
                subprocess.run(command, stdout=std, stderr=err)  #, check=True)
        else:
            subprocess.run(command)  # , check=True)


def main():
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('directory', help='Model directory containing ' + const.FILE_CONFIG + ' file.')
    parser.add_argument('-ot', '--omit_training', default=False, action='store_true', help='Skip training.')
    parser.add_argument('-oe', '--omit_evaluation', default=False, action='store_true', help='Skip evaluation.')
    parser.add_argument('-ng', '--naive_grid', default=False, action='store_true', help='Evaluate grid parameters independently (needs fixed default parameter from params).')
    parser.add_argument('-m', '--memory', default='12G', help='Max RAM usage for docker container')
    parser.add_argument('--cpu', default=False, action='store_true', help='Run on CPU.')
    parser.add_argument('-ro', '--redirect_output', default=False, action='store_true', help='Redirect stdout and stderr to files.')
    parser.add_argument('--clear', default=False, action='store_true', help='Delete existing output if model directory is not empty.')
    parser.add_argument('--cancel', default=False, action='store_true', help='Cancel if model directory is not empty.')
    parser.add_argument('--query', default=False, action='store_true', help='Query user if model directory is not empty.')
    args = parser.parse_args()

    if os.path.exists(os.path.join(args.directory, const.FILE_CONFIG)):
        if args.clear and not query('Sure you want to clear experiment? '):
            sys.exit()

        run_experiment(args)
    else:
        print('WARNING: '+args.directory + ' is not an experiment folder!')
        if query(
                'Sure you want to run all experiments within the tree? '
        ):
            for root, dirs, files in os.walk(args.directory):
                if const.FILE_CONFIG in files:
                    args.directory = root
                    run_experiment(args)


if __name__ == '__main__':
    main()
