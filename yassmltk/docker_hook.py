"""
This code calls the actual train and evaluate function. This hook
is encapsulated to conviently run it within a  container.
"""
import os
import argparse
import importlib

import yaml

import yassmltk.config as conf
import yassmltk.params as para
import yassmltk.constants as const


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-ot', '--omit_training', default=False, action='store_true')
    parser.add_argument('-oe', '--omit_evaluation', default=False, action='store_true')
    args = parser.parse_args()

    config = conf.Config.init_from_file(const.CTR_PATH_EXP, check=False)
    config.update_directories(const.CTR_PATH_OUT)

    name = const.DIR_COPY_SRC + '.' + config[const.KEY_MODULE]
    module = importlib.import_module(name)

    if not args.omit_training:
        train_params = config.copy()
        params = para.Params.init_from_file(const.CTR_PATH_OUT)
        train_params.update(params)
        module.train(train_params)

    if not args.omit_evaluation:
        eval_params = config.copy()
        params = para.Params.init_from_file(const.CTR_PATH_OUT)
        eval_params.update(params)
        module.evaluate(eval_params)
