import os
import subprocess


def get_revision_hash(path=None, short=True):

    if path is not None:
        wd = os.getcwd()
        os.chdir(path)

    if short:
        h = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'])
    else:
        h = subprocess.check_output(['git', 'rev-parse', 'HEAD'])

    if path is not None:
        os.chdir(wd)

    return h


def get_status_info(path=None):

    if path is not None:
        wd = os.getcwd()
        os.chdir(path)

    info = subprocess.check_output(['git', 'status', '--short', '--show-stash', '--porcelain=v2'])

    if path is not None:
        os.chdir(wd)

    return info


def get_diff(path=None):

    if path is not None:
        wd = os.getcwd()
        os.chdir(path)

    info = subprocess.check_output(['git', 'diff'])

    if path is not None:
        os.chdir(wd)

    return info


def write_repo_info(directory, path=None, diff=True):
    first_line = path or os.getcwd()
    with open(os.path.join(directory, 'repo_info.txt'), 'ab') as fobj:
        fobj.write(b'\n')
        fobj.write(first_line.encode('ascii'))
        fobj.write(b'\n')
        fobj.write(get_revision_hash(path, short=False))
        fobj.write(get_status_info(path))
        fobj.write(b'\n'+b'='*80+b'\n')
    with open(os.path.join(directory, 'repo_diff.txt'), 'ab') as fobj:
        fobj.write(b'\n')
        fobj.write(get_diff(path))
        fobj.write(b'\n'+b'='*80+b'\n')


if __name__ == '__main__':
    write_repo_info('./repo_info')
