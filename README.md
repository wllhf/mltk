# Yet another simple, stupid ML toolkit

#### Prerequisites
- Python 3.7 or higher
- Pandas
- PyYaml

### Purpose
Run ML experiments *without technical debt*.

Only minimal assumptions are made:
1. An experiment is defined by a folder containing a config.yml, params.yml and
optionally a grid.yml.

2. Model and training and evaluation processes are defined by one Python module,
which contains a *train(params)*, and a *evaluate(params)* function, whereas params
is a dictionary.

3. A docker image with pyyaml installed (see example Dockerfile).

### What YASSMLTK does for you
 - Executes training and evaluation of your model within a docker container.
 - Logs source code and parameters.
 - Does a grid search if grid.yml exists. Optionally you can do a naive grid search
 with default parameter choice per dimension.
 
### Install YASSMLTK
You don't need to install it. Just run 

    python /path/to/repo/yassmltk/run_exp.py /path/to/experiment_folder
    
You can install it though. Just run in repo root:

    pip install ./
    
After installation you can run YASSMLTK with:

    runexp /path/to/experiment_folder
  
### How to use YASSMLTK
see example. For additional options run

    runexp --help

